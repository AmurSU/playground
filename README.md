# Playground

Playground of amursu developer team.

Version list:

* Ruby - 3.1.4
* Rails - 7.0.4
* PostgreSQL - 1.5
* Vite - 4.5.0
* VueJS - 3.4.14
* Vuetify - 3.4.2
* Yarn - 1.22.19
* TS - 5.3.3
* Pinia - 2.1.7