class User < ApplicationRecord

  has_many :posts, dependent: :destroy

  enum sex: {male: 0, female: 1}
end
