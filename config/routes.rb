Rails.application.routes.draw do

  namespace :api, defaults: { format: :json} do
    resources :users do
      resources :posts
    end
  end


  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "application#index"
  #get 'app', to: 'application#index'
end
