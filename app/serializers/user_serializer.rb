class UserSerializer
  include JSONAPI::Serializer
  attributes :id, :first_name, :last_name, :token, :age, :sex

  has_many :posts
end
