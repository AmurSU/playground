class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :token, default: "", null: false
      t.integer :age, default: 0, null: false
      t.integer :sex, default: 0, null: false

      t.timestamps
    end
  end
end
