# frozen_string_literal: true

# class PostSerializer < ActiveModel::Serializer
#   attributes :title, :body
# end

class PostSerializer

  include JSONAPI::Serializer

  attributes :title, :body

  belongs_to :user
end
