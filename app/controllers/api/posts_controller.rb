class Api::PostsController < ApplicationController
  # before_action :set_post, only: %i[ show edit update destroy ]

  # GET /posts or /posts.json
  def index
    options = { include: [:user] }
    render json: PostSerializer.new(Posts::ListPosts.run!, options).serializable_hash.to_json
  end

  # GET /posts/1 or /posts/1.json
  def show
    render json: PostSerializer.new(post).serializable_hash.to_json
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts or /posts.json
  def create
    outcome = Posts::CreatePost.run(params.fetch(:post, {}))

    if outcome.valid?
      redirect_to(outcome.result)
    else
      @post = outcome
      render(:new)
    end

  end

  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to post_url(@post), notice: "Post was successfully updated." }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1 or /posts/1.json
  def destroy
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url, notice: "Post was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def post
    outcome = Posts::FindPost.run params

    if outcome.valid?
      @_post ||= outcome.result
    else
      fail ActiveRecord::RecordNotFound, outcome.errors.full_messages.to_sentence
    end
  end

  # Only allow a list of trusted parameters through.
  def post_params
    params.require(:post).permit(:title, :body)
  end
end
