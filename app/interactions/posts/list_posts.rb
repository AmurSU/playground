# frozen_string_literal: true
module Posts
  class ListPosts < ActiveInteraction::Base

    def execute
      Post.all
    end

  end
end

