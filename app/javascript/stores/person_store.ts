import { defineStore } from 'pinia'
import { ref } from 'vue'

export const usePersonStore = defineStore('person ', () => {
        const name = ref('Mike');

        const getPerson = () => {
            name.value = 'Max';
        }

        return {
            name,
            getPerson
        }
})

