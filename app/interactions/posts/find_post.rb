# frozen_string_literal: true
module Posts
  class FindPost < ActiveInteraction::Base
    integer :id

    def execute
      post = Post.find_by_id id

      if post
        post
      else
        errors.add(:id, 'does not exist')
      end
    end

  end
end

